<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

if (!empty($_POST["password"]))
	{
   $password = MyClass::data_filter($_POST['password']);
   $hashed_salt = MyClass::generate_password_salt();
   $hashed_password = MyClass::encrypt_password($password,$hashed_salt);
   $statement = $DBconnection->prepare('UPDATE admin SET password = :password, passwordsalt = :passwordsalt WHERE id = :id');
   $statement->bindParam(':password', $hashed_password, PDO::PARAM_STR); 
   $statement->bindParam(':passwordsalt', $hashed_salt, PDO::PARAM_STR); 
   $statement->bindParam(':id', $AdminID, PDO::PARAM_STR); 
   $statement->execute();
	}
echo "success <script>alert('Updated successfully!');</script>";
echo "<script>window.location.href='admin-account.php';</script>";
?>