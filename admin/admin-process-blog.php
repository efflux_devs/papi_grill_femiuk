<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$title = MyClass::data_filter($_POST['title']);
$category = MyClass::data_filter($_POST['category']);
$content = $_POST['content'];
$insert_data = MyClass::data_filter($content);
while(($pos = stripos($insert_data,"<script"))!==false){
    $end_pos = stripos($insert_data,"</script>");
    $start = substr($insert_data, 0, $pos);
    $end = substr($insert_data, $end_pos+strlen("</script>"));
    $insert_data = $start.$end;
}
//Start Update Photo Changes
$fileData = "image";
$filePath = "uploads";
$fileType = "photo";
$fileuploadcheck = MyClass::userfileupload_check($fileData,$filePath,$fileType);

if ($fileuploadcheck =="success") 
        {
$newname = MyClass::userfileupload_post($fileData,$filePath,$fileType);
		} else {
echo "$fileuploadcheck";
exit();
		}
//End Upload Photo Changes
$newdate = date("d-m-Y");

$query = "INSERT INTO blog SET title=:title, category=:category, content=:content, image=:image, author=:author, date=:date";
$statement = $DBconnection->prepare($query);
$statement->bindParam(':title', $title, PDO::PARAM_STR); 
$statement->bindParam(':category', $category, PDO::PARAM_STR); 
$statement->bindParam(':content', $content, PDO::PARAM_STR); 
$statement->bindParam(':image', $newname, PDO::PARAM_STR);  
$statement->bindParam(':author', $_SESSION['AdminName'], PDO::PARAM_STR); 
$statement->bindParam(':date', $newdate, PDO::PARAM_STR); 
$statement->execute();
echo "success <script>alert('Posted successfully!');</script>";
echo "<script>window.location.href='admin-blog.php';</script>";
?>