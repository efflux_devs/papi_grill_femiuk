<div class="sidebar" data-color="red" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="admin-pages.php" class="simple-text">
                    <img src="assets/img/logo.png">
					Admin Portal
                </a>
            </div>

            <ul class="nav">
                <li <?PHP if($newpage=="overview"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-pages.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
				<li <?PHP if($newpage=="blog"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-blog.php">
                        <i class="pe-7s-menu"></i>
                        <p>Blog</p>
                    </a>
                </li>
				<li <?PHP if($newpage=="pages"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-gallery.php">
                        <i class="pe-7s-menu"></i>
                        <p>Gallery</p>
                    </a>
                </li>
				<li <?PHP if($newpage=="pages"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-pages.php">
                        <i class="pe-7s-menu"></i>
                        <p>Pages</p>
                    </a>
                </li>
				<li <?PHP if($newpage=="pages"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-newsletter.php">
                        <i class="pe-7s-menu"></i>
                        <p>Newsletter</p>
                    </a>
                </li>
                <li <?PHP if($newpage=="pages"){ ?>class="active"<?PHP } ?>>
                    <a href="admin-banners.php">
                        <i class="pe-7s-menu"></i>
                        <p>Banners</p>
                    </a>
                </li>
				<li class="active-pro">
                    <a href="<?PHP echo "$SiteURL"; ?>logout">
                        <i class="pe-7s-close"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
    	</div>
</div>