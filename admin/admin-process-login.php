<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

$Username = MyClass::data_filter($_POST['username']);
$Password = MyClass::data_filter($_POST['password']);

//Get password password salt from database and use to hash submitted password
$statement = $DBconnection->prepare("SELECT * FROM admin WHERE email = :query");
$statement->bindValue(':query', $Username);
$statement->execute();
$roow = $statement->fetch(PDO::FETCH_ASSOC);
$PassSalt = $roow['passwordsalt'];
$ID = $roow['id'];
$Name = $roow['username'];
$Email = $roow['email'];
$Type = $roow['type'];
$Hashed_Password = MyClass::encrypt_password($Password,$PassSalt);

//Use the password and login credentials to validate user details in the database
$statement = $DBconnection->prepare("SELECT * FROM admin WHERE email = :queryone AND password = :querytwo");
$statement->bindParam(':queryone', $Username, PDO::PARAM_STR);       
$statement->bindParam(':querytwo', $Hashed_Password, PDO::PARAM_STR); 
$statement->execute();
$ResultCount = $statement->rowCount();

//If Result Count is equal to 1 row, then login is successful. Now fetch user info from database and send to frontend via JSON data
if($ResultCount==1){
session_regenerate_id();
$_SESSION['AdminID'] = "$ID";
$_SESSION['AdminName'] = "$Name";
$_SESSION['AdminEmail'] = "$Email";
$_SESSION['AdminType'] = "$Type";
setcookie("AdminID", $ID, time() + (86400 * 30), "/"); // 86400 = 1 day
echo "success<script>window.location.href='admin-pages.php';</script>";
exit;
} else { 
echo "success<script>window.location.href='admin-invalidlogin.php';</script>";
}
?>