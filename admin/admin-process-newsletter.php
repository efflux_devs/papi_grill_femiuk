<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$title = MyClass::data_filter($_POST['title']);
$to = MyClass::data_filter($_POST['to']);
$content = $_POST['content'];
$insert_data = MyClass::data_filter($content);
while(($pos = stripos($insert_data,"<script"))!==false){
    $end_pos = stripos($insert_data,"</script>");
    $start = substr($insert_data, 0, $pos);
    $end = substr($insert_data, $end_pos+strlen("</script>"));
    $insert_data = $start.$end;
}
$newdate = date("d-m-Y");

// send successfully newsletter email
$to = "$to";
$subject = "$title";
$body = "$content";
$myautomailresult = MyClass::automatedmail($to,$subject,$body);

if ($myautomailresult == "success") {
    $formsuccessmsg = "New email sent successfully";
} else {
    $formerrormsg = "Failure in sending email: $myautomailresult";
	}
// end successful newsletter email	
echo "success <script>alert('Sent successfully!');</script>";
echo "<script>window.location.href='admin-newsletter.php';</script>";
?>