<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$pageid = MyClass::data_filter($_POST['pageid']);
$content = $_POST['content'];
$insert_data = MyClass::data_filter($content);
while(($pos = stripos($insert_data,"<script"))!==false){
    $end_pos = stripos($insert_data,"</script>");
    $start = substr($insert_data, 0, $pos);
    $end = substr($insert_data, $end_pos+strlen("</script>"));
    $insert_data = $start.$end;
}

if($_FILES["image"]['tmp_name'] != ''){
//Start Update Photo Changes
$fileData = "image";
$filePath = "uploads";
$fileType = "photo";
$fileuploadcheck = MyClass::userfileupload_check($fileData,$filePath,$fileType);

if ($fileuploadcheck =="success") 
        {
$newname = MyClass::userfileupload_post($fileData,$filePath,$fileType);
		} else {
echo "$fileuploadcheck";
exit();
		}
//End Upload Photo Changes
$query = "UPDATE pages SET pagebanner=:pagebanner WHERE id=:id";
$statement = $DBconnection->prepare($query);
$statement->bindParam(':pagebanner', $newname, PDO::PARAM_STR); 
$statement->bindParam(':id', $pageid, PDO::PARAM_STR);  
$statement->execute();
}

$newdate = date("d-m-Y");

$query = "UPDATE pages SET content=:content, lastupdated=:lastupdated, author=:author WHERE id=:id";
$statement = $DBconnection->prepare($query);
$statement->bindParam(':content', $content, PDO::PARAM_STR); 
$statement->bindParam(':lastupdated', $newdate, PDO::PARAM_STR); 
$statement->bindParam(':author', $_SESSION['AdminName'], PDO::PARAM_STR); 
$statement->bindParam(':id', $pageid, PDO::PARAM_STR);  
$statement->execute();
echo "success <script>alert('Saved successfully!');</script>";
echo "<script>window.location.href='admin-pages.php';</script>";
?>