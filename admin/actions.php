<?PHP
use Classes\General\MyClass;

if($_SESSION['AdminType']!="super"){
echo"<div class='main-panel'><div class='content'><div class='row'><div class='col-md-12'><h4 class='title'>Access denied. This action is reserved for Super Admins only!</h4></div></div></div></div>";
exit();
}

if($secondquery=="delete_gallery")
 {
$statement = $DBconnection->prepare("DELETE FROM gallery WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_customer")
 {
$statement = $DBconnection->prepare("DELETE FROM users WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_driver")
 {
$statement = $DBconnection->prepare("DELETE FROM drivers WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_admin")
 {
$statement = $DBconnection->prepare("DELETE FROM admin WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_truck")
 {
$statement = $DBconnection->prepare("DELETE FROM vehicles WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_service")
 {
$statement = $DBconnection->prepare("DELETE FROM services WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_route")
 {
$statement = $DBconnection->prepare("DELETE FROM routes WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } elseif($secondquery=="delete_blog")
 {
$statement = $DBconnection->prepare("DELETE FROM blog WHERE id=:id");
$statement->bindParam(':id', $firstquery, PDO::PARAM_STR); 
$statement->execute();
 } 

echo "<div class='main-panel'><div class='content'><div class='row'><div class='col-md-12'><h4 class='title'>Action successful!</h4></div></div></div></div>";
?>