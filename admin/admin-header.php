<div class="collapse navbar-collapse" id="PageHeaderMenu">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret"></b>
                                    <span class="notification"><?php echo"$NotificationsCount"; ?></span>
                              </a>
                              <ul class="dropdown-menu">
<?php
$stmt = $DBconnection->prepare("SELECT * FROM notifications WHERE 'status'='unread' ORDER BY id DESC");
$stmt->execute();
$NotificationsCount = $stmt->rowCount();

while($notificationsrow = $stmt->fetch(PDO::FETCH_ASSOC))
	  {
$notificationsdata_id = $notificationsrow['id'];
$notificationsdata_from = $notificationsrow['from'];
$notificationsdata_to = $notificationsrow['to'];
$notificationsdata_status = $notificationsrow['status'];
$notificationsdata_title = $notificationsrow['title'];
$notificationsdata_details = $notificationsrow['details'];
$notificationsdata_action = $notificationsrow['action'];
$notificationsdata_time = $notificationsrow['time'];
$notificationsdata_date = $notificationsrow['date']; 
?>							  
                                <li><a href="admin-notifications.php#<?php echo"$notificationsdata_id"; ?>"><?php echo"$notificationsdata_title"; ?></a></li>
<?php } ?>	
                                <li><a href="admin-notifications.php">View all</a></li>
                              </ul>
                        </li>
                        <li>
                           <a href="#">
                                <i class="fa fa-search"></i>
                          </a>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                           <a href="admin-account.php">
                               Account
                            </a>
                        </li>
                        <!--<li class="dropdown">
                              <a href="<?PHP echo "$SiteURL"; ?>admin/shipments" class="dropdown-toggle" data-toggle="dropdown">
                                    Shipments
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="<?PHP echo "$SiteURL"; ?>admin/shipments">All Shipments</a></li>
                                <li><a href="<?PHP echo "$SiteURL"; ?>admin/shipments/In-transit">In-transit Shipments</a></li>
                                <li><a href="<?PHP echo "$SiteURL"; ?>admin/shipments/Delivered">Delivered Shipments</a></li>
                                <li><a href="<?PHP echo "$SiteURL"; ?>admin/shipments/Returned">Returned Shipments</a></li>
                                <li class="divider"></li>
                                <li><a href="<?PHP echo "$SiteURL"; ?>admin/manage_delivery">Manage Shipment</a></li>
                              </ul>
                        </li> -->
                        <li>
                            <a href="admin-logout.php">
                                Logout
                            </a>
                        </li>
                    </ul>
</div>