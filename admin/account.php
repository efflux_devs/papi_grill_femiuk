<!--	MAIN CONTENT 	-->
<div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">-->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Edit Admin Details</a>
                </div>
                <? include 'admin-header.php'; ?>
            </div>
        </nav>


        <div class="content">
                                <form name="AdminEditAccountForm" id="AdminEditAccountForm">	
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input name="password" id="password" type="password" class="form-control" placeholder="password" value="">
                                            </div>
                                        </div>
                                    </div>
									
									<div class="alert"></div>
				                    <input type="hidden" id="action" name="action" value="update_admin_details">
                                    <button id="AdminEditAccount_BTN" type="submit" class="btn btn-info btn-fill pull-right">Save</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>

</div> 