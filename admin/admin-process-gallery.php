<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$postid = MyClass::data_filter($_POST['postid']);
$title = MyClass::data_filter($_POST['title']);
$type = MyClass::data_filter($_POST['type']);
$tags = MyClass::data_filter($_POST['tags']);
$details = MyClass::data_filter($_POST['details']);

//Start Update Photo Changes
$fileData = "media";
$filePath = "uploads";
$fileType = "$type";
$fileuploadcheck = MyClass::userfileupload_check($fileData,$filePath,$fileType);

if ($fileuploadcheck =="success") 
        {
$newname = MyClass::userfileupload_post($fileData,$filePath,$fileType);
		} else {
echo "$fileuploadcheck";
exit();
		}
//End Upload Photo Changes
$newdate = date("d-m-Y");

$query = "INSERT INTO gallery SET postid=:postid, type=:type, caption=:title, tags=:tags, details=:details, media=:image, author=:author, date=:date";
$statement = $DBconnection->prepare($query);
$statement->bindParam(':postid', $postid, PDO::PARAM_STR); 
$statement->bindParam(':type', $type, PDO::PARAM_STR); 
$statement->bindParam(':title', $title, PDO::PARAM_STR); 
$statement->bindParam(':tags', $tags, PDO::PARAM_STR); 
$statement->bindParam(':details', $details, PDO::PARAM_STR); 
$statement->bindParam(':image', $newname, PDO::PARAM_STR);  
$statement->bindParam(':author', $_SESSION['AdminName'], PDO::PARAM_STR); 
$statement->bindParam(':date', $newdate, PDO::PARAM_STR); 
$statement->execute();
echo "success <script>alert('Posted successfully!');</script>";
echo "<script>window.location.href='admin-gallery.php';</script>";
?>