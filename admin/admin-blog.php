<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

//Page Model
if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$stmt = $DBconnection->prepare("SELECT * FROM notifications WHERE 'to'=:id AND 'status'='unread' ORDER BY id DESC");
$stmt->bindParam(':id', $AdminID); 
$stmt->execute();
$NotificationsCount = $stmt->rowCount();
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Admin Portal</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
	
	<!-- Include JQuery Library -->
	<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="assets/css/toast.css" rel="stylesheet" />
	
    <!--Time Picker-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	
	<!--Select SearchSorter-->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	
<!-- Modal PopUp Style -->
<style>
/* The Modal (background) */
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1000; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 12px;
    border: 0px solid #888;
    width: 80%;
}

/* The Close Button */
.close {
    color: #aaaaaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}
</style>
<script type="text/javascript" src="assets/js/custom-functions.js"></script>
<script src="//cdn.ckeditor.com/4.5.4/full/ckeditor.js"></script>
</head>
<body>

<div class="wrapper">
<?php include 'admin-side.php'; ?>

<!--	MAIN CONTENT 	-->
<div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Blog</a>
                </div>
                <? include 'admin-header.php'; ?>
            </div>
        </nav>


<div class="content">
		
<!--<form>
<input id="searchquery" name="searchquery" type="text" size="40" />
<label>
<input type="hidden" name="searchpage" id="searchpage" value="gallery" />
<input type="button" onClick="siteAdminSearch();" value="Search" />
</label>
</form>-->	
		
<div class="row">
<h4 class="title">Post New Blog Post</h4>
<br>
<form action="admin-process-blog.php" method="post" enctype="multipart/form-data">
 <div class="col-md-12">
  <div class="form-group">
    <label>Title</label>
    <input type="text" name="title" id="title" class="form-control" placeholder="Title">
    </div>
 </div>
 
 <div class="col-md-12">
  <div class="form-group">
    <label>Category</label>
    <select id="category" name="category" class="form-control">
    <option value="blog">Blog</option>
	<option value="news">News</option>
	<option value="events">Events</option>
	<option value="artseriez">ArtSeriez</option>
	</select>
    </div>
 </div>
 
 <div class="col-md-12">
  <div class="form-group">
    <label>Image</label>
    <input name="image" type="file" id="image" class="form-control">
    </div>
 </div>
 
 <div class="col-md-12">
  <div class="form-group">
    <label>Details</label>
	<textarea name="content" class="ckeditor"></textarea>
    </div>
 </div>
 
<div class="alert"></div>
<input type="hidden" id="action" name="action" value="post_blog">
<button id="CreateBlogBTN" type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Add Media (Opens Pop-up)</button>
<div class="clearfix"></div>
</form>
</div>		

<div class="row">
 <div class="col-md-12">
 <br /><hr /><br />
 </div>
</div>

<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><? echo "$PageTitle"; ?></h4>
                                <p id="category" class="category"></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Title</th>
                                    	<th>Category</th>
                                    	<th>Author</th>
                                    	<th>Date</th>
                                    	<th>Action</th>
                                    </thead>
                                    <tbody>
<?php
$stmt = $DBconnection->prepare("SELECT * FROM blog ORDER BY id DESC");
$stmt->execute();
//$ProductQueryClauseCount = $stmt->rowCount();
while($info = $stmt->fetch(PDO::FETCH_ASSOC))
	  {	 
?>
                                        <tr>
                                        	<td><a href="#" target="_blank"><? Print $info['id']; ?></a></td>
                                        	<td><? Print $info['title']; ?></td>
                                        	<td><? Print $info['category']; ?></td>
                                        	<td><? Print $info['author']; ?></td>
                                        	<td><? Print $info['date']; ?></td>
											<td><a href="admin-actions.php?id=<? Print $info['id']; ?>&action=delete_blog">Delete</a></td>
                                        </tr>
<? } ?>										
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>            							
        </div>
</div>
<!--End MainContent -->
<?php include 'admin-media-widget.php'; ?>

</div>

    <!--   Core JS Files   -->
    <!--<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>-->
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <!--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>-->

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<!--<script src="assets/js/demo.js"></script>-->

	
<script src="assets/js/functions-custom.js"></script>
<script src="assets/js/toast.js"></script>
<script>
//TimePicker
	$('.timepicker').timepicker({
    timeFormat: 'h:mm p',
    interval: 60,
    minTime: '10',
    maxTime: '11:00pm',
    defaultTime: '11',
    startTime: '1:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
    });

	<!--Select SearchSorter Footer-->
	// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
    });
</script>
<?php include 'admin-media-widget.php'; ?>
</body>
</html>