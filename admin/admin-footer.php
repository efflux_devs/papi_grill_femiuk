<footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="admin-pages.php">
                                Home
                            </a>
                        </li>
						<li>
                            <a href="admin-account.php">
                                Account
                            </a>
                        </li>
                        <li>
                            <a href="admin-blog.php">
                               Blog
                            </a>
                        </li>
						<li>
                            <a href="admin-newsletter.php">
                               Newsletter
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; 2018 Papi's Grill
                </p>
            </div>
</footer>