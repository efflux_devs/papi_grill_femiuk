<?PHP
class MyClass {
 private $DatabaseHost = "localhost";
 private $DatabaseName = "";
 private $DatabaseUserName = "root";
 private $DatabaseUserPassword = "mysql";
 protected $DBconnection;
   
   public static function connectdatabase() {
   $this->DBconnection = new PDO("mysql:host=$this->DatabaseHost;dbname=$this->DatabaseName", $this->DatabaseUserName, $this->DatabaseUserPassword);
   $this->DBconnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   $this->DBconnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); // always disable emulated prepared statement when using the MySQL driver
   return $this->DBconnection;
   }
   
   public function getDatabaseHost(){ 
   return $this->DatabaseHost;
   }
   
   public function getDataFromDB($sql, $params) { //Function to fetch SQL and Params from Database
   $statement = $this->DBconnection->prepare($sql);
   $statement->execute($params);
   return $statement;
   }
   
   public function postDataToDB($sql, $params) { //Function to post SQL and Params to Database and returns LastInsertID
   $statement = $this->DBconnection->prepare($sql);
   $statement->execute($params);
   return $this->DBconnection->lastInsertId();
   }
   
   public static function data_filter($data) { //Function to filter user text input against attack for security
    // remove whitespaces from begining and end
    $data = trim($data);
    
    // apply stripslashes to pevent double escape if magic_quotes_gpc is enabled
    // connection is required before using this function
    $data=strip_tags($data);
	// Remove HTML tags from string
    $data = filter_var($data, FILTER_SANITIZE_STRING);
    return $data;
   }
   
   public static function generate_password_salt() { //Function to generate password hash salt
    $salt = uniqid(sha1("L0r3mIpsUmD0l0rS1tAm3tc0ns3CT3tur4d1p1sc1ng3lit".microtime()));
	$salt = substr(sha1($salt), 0, 22);
	return $salt;
   }
   
   public static function encrypt_password($password,$salt){ //Function to encrypt password with generated salt hash
   return hash('sha256', $password . $salt); // can use also different algorithm like sha512 or whirlpool
   }
   
   public static function generate_numcode($length) { //Function to generate random numeric code
    $length = trim($length); // remove whitespaces from begining and end
    
    $string = "0123456789";
    $code = "";
    for($i=0; $i<$length; $i++){
    $y = rand(0,strlen($string)-1);
    $code .= $string[$y];
    }
    return $code;
   }
  
  public static function generate_code($length) { //Function to generate random codes
    $length = trim($length); // remove whitespaces from begining and end
    
    $string = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $code = "";
    for($i=0; $i<$length; $i++){
    $y = rand(0,strlen($string)-1);
    $code .= $string[$y];
    }
    return $code;
  }
  
  public static function form_optionlist($label,$mydbh) { //Function to generate form dropdown options list
    if($label=="teams"){
	$sitemenusubcategoryfetchdata_sql= "SELECT * FROM teams ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($categoriesformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    { 
     $categoriesdataid = $categoriesformdatarow['id'];
     $categoriesdataname = $categoriesformdatarow['team'];
	 
	 $List_array[] = "<option value='$categoriesdataname'>$categoriesdataname</option>";
    }
	
	} elseif($label=="leagues"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM leagues ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($sizeformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    { 
     $sizedataid = $sizeformdatarow['id'];
     $sizedataname = $sizeformdatarow['league'];
	 
	 	 $List_array[] = "<option value='$sizedataname'>$sizedataname</option>";
    }
	
	} elseif($label=="predictions"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM predictions ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($colorformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    {  
     $colordataid = $colorformdatarow['id'];
     $colordatacompanyname = $colorformdatarow['prediction'];
	 
	 	 $List_array[] = "<option value='$colordatacompanyname'>$colordatacompanyname</option>";
    }
	
	} elseif($label=="departmentsnum"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM departments WHERE companyid = '$CompanyID' ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($sizeformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    {  
     $sizedataid = $sizeformdatarow['id'];
     $sizedataname = $sizeformdatarow['department'];
	 
	 	 $List_array[] = "<option value='$sizedataid'>$sizedataname</option>";
    }
	
	} elseif($label=="subcategories"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM subcategories WHERE companyid = '$CompanyID' ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($subcategoriesformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    { 
     $subcategorydataid = $subcategoriesformdatarow['id'];
     $subcategorydataname = $subcategoriesformdatarow['subcategory'];
	 
	 $List_array[] = "<option value='$subcategorydataname'>$subcategorydataname</option>";
    }
	
	} elseif($label=="groupsnum"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM groups WHERE companyid = '$CompanyID' ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($subcategoriesformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    { 
     $subcategorydataid = $subcategoriesformdatarow['id'];
     $subcategorydataname = $subcategoriesformdatarow['groupname'];
	 
	 $List_array[] = "<option value='$subcategorydataid'>$subcategorydataname</option>";
    }
	
	} elseif($label=="products"){
    $sitemenusubcategoryfetchdata_sql= "SELECT * FROM products WHERE companyid = '$CompanyID' ORDER BY id ASC";
    $sitemenusubcategoryfetchdata = $mydbh->prepare($sitemenusubcategoryfetchdata_sql);
    $sitemenusubcategoryfetchdata->execute();
    while($subcategoriesformdatarow = $sitemenusubcategoryfetchdata->fetch(PDO::FETCH_ASSOC))
    { 
     $subcategorydataid = $subcategoriesformdatarow['id'];
     $subcategorydataname = $subcategoriesformdatarow['productname'];
	 
	 $List_array[] = "<option value='$subcategorydataid'>$subcategorydataname</option>";
    }
	
	} 
	
   $ListOptions = implode(" ", $List_array);
   return $ListOptions;
   }
   
   public static function userfileupload_check($fileData,$filePath,$fileType){ //Function to check user file upload format and size for verification
       if(!$_FILES["$fileData"]['name']) {
       $filecheckresult.= "No file specified";
       }
	   
	   //Get file extension
	   $file_title = $_FILES["$fileData"]['name'];
       $i = strrpos($file_title,".");
       $l = strlen($file_title) - $i;
       $extension = substr($file_title,$i+1,$l);
	   
	   if($fileType == "photo") {

          if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
 		  {
          $filecheckresult.= "Un-supported extension! Your extension is $extension";
 		  }
       } elseif($fileType == "video") {

          if ($extension != "mp4") 
 		  {
          $filecheckresult.= "Un-supported extension! Your extension is $extension";
 		  }
       } elseif($fileType == "audio") {

          if ($extension != "mp3") 
 		  {
          $filecheckresult.= "Un-supported extension! Your extension is $extension";
 		  }
       } elseif($fileType == "file") {

          if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "JPEG") && ($extension != "png") && ($extension != "PNG") && ($extension != "gif") && ($extension != "GIF") && ($extension != "mp4") && ($extension != "mp3") && ($extension != "pdf") && ($extension != "doc") && ($extension != "docx") && ($extension != "xls") && ($extension != "xlsx") && ($extension != "ppt") && ($extension != "pptx") && ($extension != "zip") && ($extension != "txt") && ($extension != "tar") && ($extension != "tar.gz")) 
 		  {
          $filecheckresult.= "Un-supported extension! Your extension is $extension";
 		  }
       } elseif($fileType == "game") {

          if (($extension != "swf") && ($extension != "flv")) 
 		  {
          $filecheckresult.= "Un-supported extension! Your extension is $extension";
 		  }
       }
	   
	   $maxsize = 20097152;
       if ($_FILES["$fileData"]['size'] == 0)
       {
       $filecheckresult.= "Empty file";
       }
       if ($_FILES["$fileData"]['size'] >= $maxsize)
       {
       $filecheckresult.= "You have exceeded the allowed file size limit";
       }

       if (empty($filecheckresult))
	   {
	   $myfinalresult= "success";
	   } else {
	   $myfinalresult= "$filecheckresult";
	   }
   
   return $myfinalresult;
   }
   
  public static function userfileupload_post($fileData,$filePath,$fileType) { //Function to upload the user file to the server
    $target_filename = basename($_FILES["$fileData"]["name"]);
    $newfileName = str_replace(' ', '-', strtolower(basename($_FILES["$fileData"]["name"])));
    $target_path = $_SERVER['DOCUMENT_ROOT'] . "/$filePath/" . $newfileName;
	$fileDataTemp = $_FILES["$fileData"]['tmp_name'];
    while(file_exists($target_path)){
	   $fileExtension = pathinfo($fileDataTemp, PATHINFO_EXTENSION);
	   $newfileName = rand(10,100) . str_replace(' ', '-', strtolower($target_filename));
       $target_path = $_SERVER['DOCUMENT_ROOT'] . "/$filePath/" . $newfileName;
    } 
	
    if (move_uploaded_file($fileDataTemp, $target_path)){    // The file is in the uploads folder. return it. 
    $newname="$filePath/".$newfileName;
    }
  
  return $newname;
  }
  
  public static function mediaupload_formatcheck($fileData) { //Function to check user media file type
   if(!$_FILES["$fileData"]['name']) {
   $filecheckresult.= "No file specified";
   }
   
   $filename = $_FILES["$fileData"]['name'];
   $allowedphotos =  array('gif','png','jpeg','jpg','JPG','PNG','GIF');
   $allowedvideos =  array('mp4','3gp');
   $allowedaudios =  array('mp3','wav');
   $allowedfiles =  array('zip','doc','docx','ppt','pptx','xls','xlsx','pdf','txt');
   
   $ext = pathinfo($filename, PATHINFO_EXTENSION);
   if(in_array($ext,$allowedphotos) ) {
   $myfileType="photo"; //'na photo';
   } elseif(in_array($ext,$allowedvideos) ) {
   $myfileType="video"; //'na video';
   } elseif(in_array($ext,$allowedaudios) ) {
   $myfileType="audio"; //'na audio';
   } elseif(in_array($ext,$allowedfiles) ) {
   $myfileType="files"; //'na file';
   }
   
  return $myfileType;
  }
  
  public static function smiley($msg) { //Function to generate smiley emoji/emoticons
      $msg = str_replace(":D","<img src=images/smileys/Grin.png>", $msg); 
      $msg = str_replace(":)","<img src=images/smileys/Smile.png>", $msg); 
      $msg = str_replace(":(","<img src=images/smileys/Frown.png>", $msg); 
      $msg = str_replace(":o","<img src=images/smileys/Gasp.png>", $msg); 
      $msg = str_replace(":&","<img src=images/smileys/Sick.png>", $msg); 
      $msg = str_replace(":swag:","<img src=images/smileys/Money-Mouth.png>", $msg); 
      $msg = str_replace(":s","<img src=images/smileys/Confused.png>", $msg); 
      $msg = str_replace("B-)","<img src=images/smileys/Cool.png>", $msg); 
      $msg = str_replace("=d","<img src=images/smileys/Laughing.png>", $msg); 
      $msg = str_replace(">:o","<img src=images/smileys/Angry.png>", $msg); 
	  $msg = str_replace(">=)","<img src=images/smileys/Naughty.png>", $msg); 
      $msg = str_replace(":cry:","<img src=images/smileys/Cry.png>", $msg); 
      $msg = str_replace(":evil:","<img src=images/smileys/kill-u.png>", $msg); 
      $msg = str_replace("<3","<img src=images/smileys/Heart.png>", $msg); 
      $msg = str_replace("<3<3","<img src=images/smileys/HeartEyes.png>", $msg); 
      $msg = str_replace(":wink:","<img src=images/smileys/Wink.png>", $msg); 
      $msg = str_replace("*nerd*","<img src=images/smileys/Nerd.png>", $msg); 
      $msg = str_replace(":>","<img src=images/smileys/Sarcastic.png>", $msg); 
      $msg = str_replace("o:)","<img src=images/smileys/Innocent.png>", $msg); 
      $msg = str_replace("(y)","<img src=images/smileys/Thumbs-Up.png>", $msg); 
      $msg = str_replace("[b]","<b>", $msg); 
      $msg = str_replace("[/b] ","</b>", $msg); 
      $msg = str_replace("[u]","<u>", $msg); 
      $msg = str_replace("[/u]","</u>", $msg); 
	  $msg = str_replace("[i]","<i>", $msg); 
      $msg = str_replace("[/i] ","</i>", $msg); 
      $msg = str_replace("[center]","<center>", $msg); 
      $msg = str_replace("[/center]","</center>", $msg); 
  return $msg;
  }
  
  public static function time_ago($time) { //Function to generate time ago
   $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");

   $now = time();

       $difference     = $now - $time;
       $tense         = "ago";

   for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
       $difference /= $lengths[$j];
   }

   $difference = round($difference);

   if($difference != 1) {
       $periods[$j].= "s";
   }

  return "$difference $periods[$j] ago ";
  }
  
  public static function get_browserdevice_name($user_agent) { //Function to get browser name
   if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
    elseif (strpos($user_agent, 'Edge')) return 'Edge';
    elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
    elseif (strpos($user_agent, 'Safari')) return 'Safari';
    elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
    elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
    
   return 'Other';
  }
  
  public static function trim_text($input, $length, $ellipses = true, $strip_html = true) {
    //strip tags, if desired
    if ($strip_html) {
        $input = strip_tags($input);
    }
  
    //no need to trim, already shorter than trim length
    if (strlen($input) <= $length) {
        return $input;
    }
  
    //find last space within length
    $last_space = strrpos(substr($input, 0, $length), ' ');
    $trimmed_text = substr($input, 0, $last_space);
  
    //add ellipses (...)
    if ($ellipses) {
        $trimmed_text .= '...';
    }
  
    return $trimmed_text;
   }
  
  public static function automatedmail($to,$subject,$body,$attachmentfile=none,$attachmentname=none) { //Function to send automated mail
  // message
  $bodycontent = str_replace("\n","<br>","$body");
  $message = "
<html>
<head>
  <title>$subject</title>
</head>
<body bgcolor=#CCCCCC>
  <p><font color=#000000 face=Arial, Helvetica, sans-serif size=3>$subject</font></p>
  <table width=80% align=center cellpadding=0 cellspacing=0>
    <tr>
      <td align=center valign=middle bgcolor=#FFFFFF><table width=100% border=0 cellspacing=6 cellpadding=6>
        <tr>
          <td align=left valign=top width=100%><font color=#000000 face=Arial, Helvetica, sans-serif size=2>$bodycontent</font></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td align=center valign=middle bgcolor=#F0F0F0><table width=100% border=0 cellspacing=6 cellpadding=6>
        <tr>
          <td height=26 align=left valign=middle><font color=#333333 face=Arial, Helvetica, sans-serif size=1>Copyright &copy; 2018 Luminoso Media</font></td>
        </tr>
      </table></td>
    </tr>
</table>
</body>
</html>";

   // To send HTML mail, the Content-type header must be set
  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

  // Additional headers
  // $headers .= 'To: Teejay Maya <info@teejaymaya.com>, Boss <ceo@tosbitechnology.com>' . "\r\n";
  $headers .= 'From: luminosomedia <noreply@papisgrill.co.uk>' . "\r\n";
  $headers .= 'Reply-To: info@papisgrill.co.uk' . "\r\n";
  //$headers .= 'Cc: temitope@qedistributions.com' . "\r\n";
  //$headers .= 'Bcc: info@qship.biz' . "\r\n";

  // Mail it
  mail($to, $subject, $message, $headers);
    //$mresult="success";
    //return $mresult;	
  }  
  
  public static function get_dashboardData($label,$mydbh) { //Function to fetch activity dashboard data
  if($label=="message"){
    $siteactivitydashboardfetchdata_sql= "SELECT * FROM activitydashboard";
    $siteactivitydashboardfetchdata = $mydbh->prepare($siteactivitydashboardfetchdata_sql);
    $siteactivitydashboardfetchdata->execute();
	$activitydashboardrow = $siteactivitydashboardfetchdata->fetch(PDO::FETCH_ASSOC);
	$activitydashboard_data = $activitydashboardrow['message'];
                       }
  
  return $activitydashboard_data;
  }
  
  public static function get_DriverName($driverid,$mydbh) { //Function to fetch activity dashboard data
    $queryfetchdata_sql= "SELECT * FROM drivers WHERE id=:id";
    $queryfetchdata = $mydbh->prepare($queryfetchdata_sql);
    $queryfetchdata->bindParam(':id', $driverid, PDO::PARAM_STR); 
    $queryfetchdata->execute();
	$queryfetchdata_row = $queryfetchdata->fetch(PDO::FETCH_ASSOC);
	$queryfetchdata_data = $queryfetchdata_row['drivername'];
  
  return $queryfetchdata_data;
  } 
  
  public static function get_VehicleName($vehicleid,$mydbh) { //Function to fetch activity dashboard data
    $queryfetchdata_sql= "SELECT * FROM vehicles WHERE id=:id";
    $queryfetchdata = $mydbh->prepare($queryfetchdata_sql);
    $queryfetchdata->bindParam(':id', $vehicleid, PDO::PARAM_STR); 
    $queryfetchdata->execute();
	$queryfetchdata_row = $queryfetchdata->fetch(PDO::FETCH_ASSOC);
	$queryfetchdata_data = $queryfetchdata_row['vehicle'];
  
  return $queryfetchdata_data;
  }
  
  public static function get_ServiceName($serviceid,$mydbh) { //Function to fetch activity dashboard data
    $queryfetchdata_sql= "SELECT * FROM services WHERE id=:id";
    $queryfetchdata = $mydbh->prepare($queryfetchdata_sql);
    $queryfetchdata->bindParam(':id', $serviceid, PDO::PARAM_STR); 
    $queryfetchdata->execute();
	$queryfetchdata_row = $queryfetchdata->fetch(PDO::FETCH_ASSOC);
	$queryfetchdata_data = $queryfetchdata_row['servicetitle'];
  
  return $queryfetchdata_data;
  } 
  
  public static function get_LocationCoordinates($city, $street, $province) {
    $address = urlencode($city.','.$street.','.$province);
    $url = "https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=Nigeria";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $status = $response_a->status;

    if ( $status == 'ZERO_RESULTS' )
    {
        return FALSE;
    }
    else
    {
        $return = array('lat' => $response_a->results[0]->geometry->location->lat, 'long' => $long = $response_a->results[0]->geometry->location->lng);
        return $return;
    }
  }
  
  public static function get_LocationDrivingDistance($lat1, $lat2, $long1, $long2) {
    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response, true);
    $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

    return array('distance' => $dist, 'time' => $time);
  }
  
  public static function data_encrypt($plaintext) { 
    // Encrypt data using OpenSSL and AES-256-CBC
     $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
     $iv = openssl_random_pseudo_bytes($ivlen);
     $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
     $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
     $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );
    return $ciphertext;
  }
  
  public static function data_decrypt($ciphertext) { 
    // Decrypt data using OpenSSL and AES-256-CBC
     $c = base64_decode($ciphertext);
     $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
     $iv = substr($c, 0, $ivlen);
     $hmac = substr($c, $ivlen, $sha2len=32);
     $ciphertext_raw = substr($c, $ivlen+$sha2len);
     $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
     $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
     if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
     {
      $myoriginal_plaintext = "$original_plaintext";
     } else {
      $myoriginal_plaintext = "error";
	 }
    return $myoriginal_plaintext;
   }
   
} //End MyClass
?>