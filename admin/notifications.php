<?PHP use Classes\General\MyClass; ?>
<?php
$PageTitle="Notifications >> $firstquery";
?>

<div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Notifications (<?php echo"$firstquery"; ?>)</a>
                </div>
                <? include 'admin-header.php'; ?>
            </div>
        </nav>


<div class="content">
		
<form>
<input id="searchquery" name="searchquery" type="text" size="40" />
<label>
<input type="hidden" name="searchpage" id="searchpage" value="notifications" />
<input type="button" onClick="siteAdminSearch();" value="Search" />
</label>
</form>				

<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"><? echo "$PageTitle"; ?></h4>
                                <p id="category" class="category"></p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>From</th>
                                    	<th>To</th>
                                    	<th>Details</th>
                                    	<th>Time</th>
                                    	<th>Date</th>
                                    	<th>Status</th>
                                    </thead>
                                    <tbody>
<?php
if (!empty($firstquery)){
$stmt = $mydbh->prepare("SELECT * FROM notifications WHERE id LIKE CONCAT('%', :query, '%') OR details LIKE CONCAT('%', :query, '%') ORDER BY id DESC");
$stmt->bindValue(':query', $firstquery);
} else {
$stmt = $mydbh->prepare("SELECT * FROM notifications ORDER BY id DESC");
} 

$stmt->execute();
//$ProductQueryClauseCount = $stmt->rowCount(); 
while($info = $stmt->fetch(PDO::FETCH_ASSOC))
	  {	 
?>
                                        <tr>
                                        	<td><a name="<? Print $info['id']; ?>" id="<? Print $info['id']; ?>"><? Print $info['id']; ?></a></td>
                                        	<td><? Print $info['from']; ?></td>
                                        	<td><? Print $info['to']; ?></td>
                                        	<td><? Print $info['details']; ?></td>
                                        	<td><? Print $info['time']; ?></td>
                                        	<td><? Print $info['date']; ?></td>
											<td><? Print $info['status']; ?></td>
                                        </tr>
<? 
$statement = $DBconnection->prepare("UPDATE notifications SET status='read' WHERE id=:id");
$statement->bindParam(':id', $info['id'], PDO::PARAM_STR);  
$statement->execute();
} ?>										
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>            							
        </div>
</div>