<!-- Start Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Media Uploader</h4>
        </div>
        <div class="modal-body">
<form id="MediaUploadWidgetForm" name="MediaUploadWidgetForm">
<div class="row">
<h4 class="title">Upload Media & Add To Content Post</h4>
<br>
 <div class="col-md-12">
   <div class="form-group">
    <label>Image</label>
    <input name="media" id="media" type="file" class="form-control">
   </div>
 </div>
 <div class="col-md-12">
  <div class="form-group">
   <div class="alert"></div>
   <button id="MediaUploadWidgetBTN" type="submit" class="btn btn-info btn-fill pull-right">Upload</button>
  </div>
 </div>
 <div class="col-md-12">
   <div class="form-group">
    <label>Result</label>
    <div class="mediauploadresult"></div>
   </div>
 </div>
</div>
</form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
</div>
<!-- End Modal -->

<script>
//Start Post Media Function
$(document).ready(function() {
            $("#MediaUploadWidgetBTN").click(function(){
            var formData = new FormData($("#MediaUploadWidgetForm")[0]);
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-media-widget-result.php",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $(".alert").show(); $(".alert").html('Connecting...'); },
                    success: function(data, status){ $(".mediauploadresult").html(data); },
                    error: function(data, status, xhr){ alert("Error "+xhr.status+xhr.statusText); }
                });
  
                return false;
            });
        });
//End Post Media Function

//Start Media Upload PopUp Function
function siteSearch()
{
 $(document).ready(function() {
 var searchQ=$("#searchquery").val();
 var searchQTwo=$("#searchquerytwo").val();
 var searchPage=$("#searchpage").val();
 var dataString=searchPage+"/"+searchQ+"/"+searchQTwo;
 window.location.replace("http://polisheddeltans.com/"+dataString);
 });
} 
//End Media Upload PopUp Function
$('.modal').css('z-index', 9999);  // note: it appears 'zIndex' no longer works
$('.modal').css('position', 'absolute');
</script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->