<?PHP
session_start();
include 'library/config.php';
include 'library/functions.php';  

if(!isset($_SESSION["AdminID"])){
header('Location: logout.php');
exit();
}
$AdminID = $_SESSION['AdminID'];

$bannerid = MyClass::data_filter($_POST['bannerid']);
$bannerlink = MyClass::data_filter($_POST['bannerlink']);
$newtime = time();
$newdate = date("d-m-Y");

//Start Update Photo Changes
$fileData = "image";
$filePath = "uploads";
$fileType = "photo";
$fileuploadcheck = MyClass::userfileupload_check($fileData,$filePath,$fileType);

if ($fileuploadcheck =="success") 
        {
$newname = MyClass::userfileupload_post($fileData,$filePath,$fileType);
    } else {
echo "$fileuploadcheck";
exit();
    }
//End Upload Photo Changes

$firsttempquery = "UPDATE banners SET image=:image, link=:link, date=:date WHERE id=:id";
$statement = $DBconnection->prepare($firsttempquery);
$statement->bindParam(':image', $newname, PDO::PARAM_STR); 
$statement->bindParam(':link', $bannerlink, PDO::PARAM_STR); 
$statement->bindParam(':date', $newdate, PDO::PARAM_STR); 
$statement->bindParam(':id', $bannerid, PDO::PARAM_STR); 
$statement->execute(); 
echo "success <script>alert('Posted successfully!');</script>";
echo "<script>window.location.href='admin-banners.php';</script>";
?>