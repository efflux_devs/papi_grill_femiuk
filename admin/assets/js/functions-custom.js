if (window.location.search.indexOf('formsent=yes') > -1) {
    alert('Action successful!');
} 


//Start Login Class
function onSuccessLogin(data, status)
        {
            data = $.trim(data);
			    if (data==="success") {
				$("#snackbar").html(data); myToastFunction();
				/**var LoginDetails=$("#email").val();
                localStorage.setItem('LoginDetails', LoginDetails);
				//alert("Login successful!"); **/
                window.location.href = "index.php";
				} else {
				$("#snackbar").html(data); myToastFunction();
				//$("#p").addClass("alert alert-success");
				//alert(data);	
				}
        }
  
        function onErrorLogin(data, status)
        {
            // handle an error
			alert("Error: " + data + status);
        }        
  
        $(document).ready(function() {
            $("#loginformSubmit").click(function(){
  
                //var formdata = $("#registrationForm").serialize();
				//var formdata = new FormData(myformdata);
 var LoginEmail=$("#email").val();
 var LoginPassword=$("#password").val();
 var LoginDevice = localStorage.getItem('LoginDevice');
 var PushTokenDevice = localStorage.getItem('PushTokenDevice');
 var dataString="email="+LoginEmail+"&password="+LoginPassword+"&LoginDevice="+LoginDevice+"&PushTokenDevice="+PushTokenDevice+"&submit=SignIn";
 
 if($.trim(LoginEmail).length>0 & $.trim(LoginPassword).length>0)
 {				
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/login.php",
                    cache: false,
					crossDomain: true,
                    data: dataString,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessLogin,
                    error: onErrorLogin
                });
				
 } else { $("#snackbar").html("Please fill all form fields"); myToastFunction(); }
  
                return false;
            });
        });
//End Login Class


//Start Register Class
function onSuccessRegister(data, status)
        {
            data = $.trim(data);
			    if (data==="success") {
				//alert(data);	
			    /**$("#snackbar").html(data); myToastFunction();
				var LoginDetails=$("#REGemail").val();
                localStorage.setItem('LoginDetails', LoginDetails);**/
                window.location.href = "index.php";
				} else {
				$("#snackbar").html(data); myToastFunction();
				//$("#p").addClass("alert alert-success");
                //alert(data);	
				}
        }
  
        function onErrorRegister(data, status)
        {
            // handle an error
			alert("Error: " + data + status);
        }        
  
        $(document).ready(function() {
            $("#registrationformSubmit").click(function(){
  
                //var formdata = $("#registrationForm").serialize();
				//var formdata = new FormData(myformdata);
 var RegUsername=$("#REGusername").val();
 var REGFirstname=$("#REGfirstname").val();
 var REGLastname=$("#REGlastname").val();
 var RegEmail=$("#REGemail").val();
 var REGphone=$("#REGphone").val();
 var RegPassword=$("#REGpassword").val();
 var RegConfirmPassword=$("#REGconfirmpassword").val();
 var REGReferrer=$("#REGreferrer").val();
 var type=$("#type").val();
 var dataString="username="+RegUsername+"&firstname="+REGFirstname+"&lastname="+REGLastname+"&email="+RegEmail+"&phone="+REGphone+"&password="+RegPassword+"&confirmpassword="+RegConfirmPassword+"&type="+type+"&referrer="+REGReferrer+"&submit=SignUp";
 
 if($.trim(RegUsername).length>0 & $.trim(RegEmail).length>0 & $.trim(RegPassword).length>0)
 {				
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/process-registration.php",
                    cache: false,
					crossDomain: true,
                    data: dataString,
					beforeSend: function(){ $("#snackbar").html("Processing..."); myToastFunction();},
                    success: onSuccessRegister,
                    error: onErrorRegister
                });
				
 } else { $("#snackbar").html("Please fill all form fields"); myToastFunction(); }
  
                return false;
            });
        });
//End Register Class


//Start Password Reminder Class
function onSuccessReminder(data, status)
        {
            data = $.trim(data);
			    if (data==="success") {
				$("#snackbar").html("Password reset link sent to your email"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
        function onErrorReminder(data, status)
        {
            // handle an error
			alert("Error: " + data + status);
        }        
  
        $(document).ready(function() {
            $("#passwordreminderformSubmit").click(function(){
  
                //var formdata = $("#registrationForm").serialize();
				//var formdata = new FormData(myformdata);
 var ReminderEmail=$("#reminderemail").val();
 var dataString="email="+ReminderEmail+"&submit=PasswordReminder";
 
 if($.trim(ReminderEmail).length>0)
 {				
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/get-password.php",
                    cache: false,
					crossDomain: true,
                    data: dataString,
					beforeSend: function(){ $("#snackbar").html("Processing..."); myToastFunction(); },
                    success: onSuccessReminder,
                    error: onErrorReminder
                });
				
 } else { $("#snackbar").html("Please fill all form fields"); myToastFunction(); }
  
                return false;
            });
        });
//End Password Reminder Class


//Start Post Blog Class
function onSuccessPostBlog(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Posted successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
        function onErrorPostBlog(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#postBlogBTN").click(function(){
  
                var formData = new FormData($("#PostBlogForm")[0]);
				           //= $("#PostSecurityAlertForm").serialize();
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/process-blog.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessPostBlog,
                    error: onErrorPostBlog
                });
  
                return false;
            });
        });
//End Post Blog Class

//Start Save Page Class
function onSuccessSavePage(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Saved successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorSavePage(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#SavePageBTN").click(function(){
  
                var formData = new FormData($("#SavePageForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-save-page.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessSavePage,
                    error: onErrorSavePage
                });
  
                return false;
            });
        });
//End Save Page Class

//Start Product Post Class
function onSuccessProductPost(data, status)
        {
            data = $.trim(data);
			$("#snackbar").html(data); myToastFunction();	
        }
  
function onErrorProductPost(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#NewProductPostFormBTN").click(function(){
  
                var formData = new FormData($("#NewProductPostForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-product.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessProductPost,
                    error: onErrorProductPost
                });
  
                return false;
            });
        });
//End Product Post Class

//Start Create Category Class
function onSuccessCreateCategory(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Created successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorCreateCategory(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#CreateCategoryBTN").click(function(){
  
                var formData = new FormData($("#CreateCategoryForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-category.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessCreateCategory,
                    error: onErrorCreateCategory
                });
  
                return false;
            });
        });
//End Create Category Class

//Start Create Sub Category Class
function onSuccessCreateSubCategory(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Created successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorCreateSubCategory(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#CreateSubCategoryBTN").click(function(){
  
                var formData = new FormData($("#CreateSubCategoryForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-subcategory.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessCreateSubCategory,
                    error: onErrorCreateSubCategory
                });
  
                return false;
            });
        });
//End Create Sub Category Class

//Start Blog Post Class
function onSuccessBlogPost(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Posted successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorBlogPost(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#NewBlogPostFormBTN").click(function(){
  
                var formData = new FormData($("#NewBlogPostForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-news.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessBlogPost,
                    error: onErrorBlogPost
                });
  
                return false;
            });
        });
//End Blog Post Class

//Start Save Account Details Class
function onSuccessAccountDetails(data, status)
        {
            data = $.trim(data);
            $("#saveAccountDetailsResult").text(data);
			if (data==="success") {
				alert("Posted successfully!");
                window.location.href = "index.php";
				} else {
				alert(data);	
				}
        }
  
        function onErrorAccountDetails(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
            $("#saveaccountdetailsBTN").click(function(){
  
                var formData = new FormData($("#editaccountForm")[0]);
				            //= $("#editaccountForm").serialize();
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/process-accountedit.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#saveAccountDetailsResult").text('Posting...'); },
                    success: onSuccessAccountDetails,
                    error: onErrorAccountDetails
                });
  
                return false;
            });
        });
//End Save Account Details Class

//Start Update Banner Class
function onSuccessUpdateBanner(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Updated successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorUpdateBanner(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#UpdateBannerBTN").click(function(){
  
                var formData = new FormData($("#UpdateBannerForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-banners.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessUpdateBanner,
                    error: onErrorUpdateBanner
                });
  
                return false;
            });
        });
//End Update Banner Class

//Start Post Newsletter Class
function onSuccessPostNewsletter(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Posted successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorPostNewsletter(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#postNewsletterBTN").click(function(){
  
                var formData = new FormData($("#NewsletterForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-newsletter.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessPostNewsletter,
                    error: onErrorPostNewsletter
                });
  
                return false;
            });
        });
//End Post Newsletter Class

//Start Manage Delivery Class
function onSuccessManageDelivery(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Posted successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorManageDelivery(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#ManageDeliveryBTN").click(function(){
  
                var formData = new FormData($("#ManageDeliveryForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-deliveryupdate.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessManageDelivery,
                    error: onErrorManageDelivery
                });
  
                return false;
            });
        });
//End Manage Delivery Class

//Start Create New Shipper Class
function onSuccessCreateNewShipper(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Created successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorCreateNewShipper(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#NewShipperFormBTN").click(function(){
  
                var formData = new FormData($("#NewShipperForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-shipper.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessCreateNewShipper,
                    error: onErrorCreateNewShipper
                });
  
                return false;
            });
        });
//End Create New Shipper Class

//Start Create New Shipment Class
function onSuccessCreateNewShipment(data, status)
        {
            data = $.trim(data);
			if (data==="success") {
				$("#snackbar").html("Created successfully!"); myToastFunction();
				} else {
				$("#snackbar").html(data); myToastFunction();	
				}
        }
  
function onErrorCreateNewShipment(data, status)
        {
            // handle an error
        }        
  
        $(document).ready(function() {
           $("#NewShipmentFormBTN").click(function(){
  
                var formData = new FormData($("#NewShipmentForm")[0]);
  
                $.ajax({
                    type: "POST",
                    url: "http://papisgrill.co.uk/admin/admin-process-shipment.php?",
                    cache: false,
					crossDomain: true,
                    data: formData,
					async: true,
					contentType: false,
                    processData: false,
					beforeSend: function(){ $("#snackbar").html("Posting..."); myToastFunction(); },
                    success: onSuccessCreateNewShipment,
                    error: onErrorCreateNewShipment
                });
  
                return false;
            });
        });
//End Create New Shipment Class
