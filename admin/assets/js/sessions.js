var userlogin = localStorage.getItem('userlogin');
var userid = localStorage.getItem('userid');
var LoginToken = localStorage.getItem('LoginToken');
var LoginDevice = localStorage.getItem('LoginDevice');
var url = location.pathname;
var filename = url.substring(url.lastIndexOf('/')+1);
/**
// Login Check With Cookies
if (document.cookie.indexOf("cookid") !== -1 && filename === 'login.html') {
   //it doesnt exist
   window.location.href="index.html";
} else if (document.cookie.indexOf("cookid") === -1 && filename !== 'login.html') {
   // do something else maybe
   window.location.href = "login.html";
}
**/

// Login Check With localStorage
if (localStorage.getItem('userid') && (filename === 'login.html' || filename === 'register.html' || filename === 'passwordreminder.html')) {
   //it doesnt exist
   window.location.href="index.html";
} else if (!localStorage.getItem('userid') && (filename !== 'login.html' && filename !== 'register.html' && filename !== 'passwordreminder.html' && filename !== 'share.html')) {
   // do something else maybe
   window.location.href = "login.html";
}

// Append User Login Token To All URL API Requests
var appToken="UserID="+userid+"&UserTOKEN="+LoginToken+"&UserDEVICE="+LoginDevice;

