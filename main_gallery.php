<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Gallery - Papi's Grill</title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Candid Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
		<!-- stylesheet -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<link rel="stylesheet" type="text/css" href="css/mediaquery.css">
		<link href="css/index.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/animate.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/new_style.css" rel="stylesheet" type="text/css" media="all" />
		<!-- //stylesheet -->
		<!-- online fonts -->
		<link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
		<!-- //online fonts -->
		<!-- font-awesome-icons -->
		<link href="css/font-awesome.css" type="text/css" rel="stylesheet"> 
		<!-- //font-awesome-icons -->


		<style type="text/css">
			input {
			  position: fixed;
			  opacity: 0;
			}
			label {
			  position: absolute;
			  margin: 0;
			  padding: 0;
			  border: none;
			  outline: none;
			  background: none;
			  cursor: pointer;
			}
			label::before {
			  position: fixed;
			  z-index: 1;
			  top: 0;
			  left: 0;
			  right: 0;
			  bottom: 0;
			  background-color: rgba(18, 67, 100, .6);
			  content: '';
			  opacity: 0;
			  pointer-events: none;
			  transition: opacity 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger {
			  position: fixed;
			  top: 2em;
			  left: 2em;
			  z-index: 3;
			  width: 4em;
			  height: 3em;
			  margin: 0;
			  padding: 0;
			  transition: opacity 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger::before, label .burger .bar, label .burger::after {
			  position: absolute;
			  left: 0;
			  display: block;
			  width: 100%;
			  height: 12%;
			  background: red;
			  content: '';
			  transition: all 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger .bar {
			  top: 44%;
			}
			label .burger::before {
			  top: 0;
			  transform-origin: top left;
			}
			label .burger::after {
			  bottom: 0;
			  transform-origin: bottom left;
			}
			input:focus + label .burger, label:hover .burger {
			  opacity: 0.75;
			}
			nav {
			  position: fixed;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  z-index: 2;
			  display: flex;
			  flex-flow: column nowrap;
			  justify-content: center;
			  align-items: flex-start;
			  transform: translate3d(0, 0, 0);
			  /* material acceleration */
			  transform: translateX(-100%);
			  will-change: transform;
			  transition: transform 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			nav::before {
			  position: absolute;
			  top: 0;
			  bottom: 0;
			  left: 0;
			  z-index: -1;
			  width: 25em;
			  background: #d62956;
			  content: '';
			  transform: skewX(15deg) translateX(-100%);
			  transform-origin: bottom left;
			  will-change: transform;
			  transition: transform 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			@media (min-width: 40em) {
			  nav::before {
			    width: 40em;
			  }
			}
			nav a {
			  margin: 0.3em 0;
			  padding: 0em 2em;
			  font-size: 2em;
			  color: white;
			  text-decoration: none;
			  font-weight: 600;
			  transform: translateX(-100%);
			  transition: color 0.15s, transform 0.5s;
			  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
			}
			nav a:nth-child(1) {
			  transition-delay: 0s, 100ms;
			}
			nav a:nth-child(2) {
			  transition-delay: 0s, 150ms;
			}
			nav a:nth-child(3) {
			  transition-delay: 0s, 200ms;
			}
			nav a:nth-child(4) {
			  transition-delay: 0s, 250ms;
			}
			nav a:nth-child(5) {
			  transition-delay: 0s, 300ms;
			}
			nav a:nth-child(6) {
			  transition-delay: 0s, 350ms;
			}
			nav a:hover, nav a:focus {
			  color: black;
			}
			main {
			  overflow: hidden;
			}
			main .content {
			  transform: translate3d(0, 0, 0);
			  /* material acceleration */
			  will-change: transform, filter;
			  transition: all 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			[id="navcheck"]:checked + label::before {
			  opacity: 1;
			  pointer-events: auto;
			}
			[id="navcheck"]:checked + label .burger::before, [id="navcheck"]:checked + label .burger::after {
			  width: 141.42%;
			}
			[id="navcheck"]:checked + label .burger::before {
			  transform: rotate(45deg) translateY(-50%);
			}
			[id="navcheck"]:checked + label .burger::after {
			  transform: rotate(-45deg) translateY(50%);
			}
			[id="navcheck"]:checked + label .burger .bar {
			  transform: scale(0.1);
			}
			[id="navcheck"]:checked ~ nav {
			  transform: translateX(0);
			}
			[id="navcheck"]:checked ~ nav::before {
			  transform: skewX(15deg) translateX(0);
			}
			[id="navcheck"]:checked ~ nav a {
			  transform: translateX(0);
			}
			[id="navcheck"]:checked ~ main .content {
			  transform: translateX(3em);
			  transform-origin: left center;
			  -webkit-filter: blur(2px);
			  filter: blur(2px);
			  /* quite laggy :S */
			}
			/* helper */
			.visuallyhidden {
			  border: 0;
			  clip: rect(0 0 0 0);
			  height: 1px;
			  margin: -1px;
			  overflow: hidden;
			  padding: 0;
			  position: absolute;
			  width: 1px;
			}
			/* misc */
			body {
			  overflow-x: hidden;
			  background: #444;
			  color: white;
			  font: 1em/1.4 "lato";
			}
			main {
			  background: #3498db;
			  padding: 5% 0;
			}
			article {
			  width: 80%;
			  margin: 0 auto;
			  font-size: 1.3em;
			}
			@media (min-width: 60em) {
			  article {
			    width: 50%;
			  }
			}
			h1 {
			  font: 2.5em/1.4 "roboto condensed";
			  font-weight: 700;
			}
		</style>
	</head>
	<body>
		<!-- BEGIN HEADER -->
			<input type="checkbox" id="navcheck" role="button" title="menu">
			<label for="navcheck" aria-hidden="true" title="menu">
				<span class="burger">
					<span class="bar">
						<span class="visuallyhidden">Menu</span>
					</span>
				</span>
			</label>
			<nav id="menu">
				<img src="logo.png" style="object-fit: cover;height: 145px;" class="img-logo">
				<a href="index.php">WELCOME</a>
				<a href="about.php">OUR STORY</a>
				<a href="main_gallery.php">GALLERY</a>
				<a href="contact.php">CONTACT US</a>
			</nav>
		<!-- END HEADER -->

		<div class="gallery_img"></div>

		<div class="container project_css">
	        <div class="row">
	            <div class="col-md-12">
	                <div class="main_body">
	                    <table style="margin-bottom: 30px;">
	                        <tbody>
	                            <tr>
	                                <th style="text-transform: uppercase; color: #251021; font-family: montserratReg;" class="portfolio-header">Our Portfolio</th>
	                            </tr>
	                        </tbody>
	                    </table>

	                    <div class="row" style="margin-bottom: 30px;">
	                        <div class="col-md-3 col-sm-6 col-xs-6 outside_border">
	                            <div class="thick_border">
	                                <div class="img_9 first_party portfolio_1">
	                                    
	                                </div>
	                            </div>
	                            <a href="video.html">
	                                <div class="on_hover_effect ">
	                                    <table>
	                                        <tr>
	                                            <th class="animated fadeInLeft" style="font-family: montserratReg;">
	                                                Check Out Some Of Our Videos Here <br> <i style="font-size: 30px !important;" class="fa fa-play" aria-hidden="true"></i>
	                                            </th>
	                                        </tr>
	                                    </table>
	                                </div>
	                            </a>
	                        </div>

	                        <div class="col-md-3 col-sm-6 col-xs-6 outside_border">
	                            <div class="thick_border">
	                                <div class="img_10 sec_party portfolio_1"></div>
	                            </div>
	                            <a href="gallery.php">
	                                <div class="on_hover_effect ">
	                                    <table>
	                                        <tr>
	                                            <th class="animated fadeInLeft" style="font-family: montserratReg;">
	                                                Check Some Of Our Images Here
	                                            </th>
	                                        </tr>
	                                    </table>
	                                </div>
	                            </a>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
		 
		<!-- copy-right -->
		<div class="copy-right agileits-w3layouts">
			<div class="container">
				<div class="social-icons agileits">
		     		<ul>
						<li><a href="https://www.facebook.com/papis.grill.7" target="_blank" class="fa fa-facebook icon icon-border facebook"> </a></li>
						<li><a href="https://twitter.com/PapisGrilLondon?s=08" target="_blank" class="fa fa-twitter icon icon-border twitter"> </a></li>
						<li><a href="https://www.instagram.com/p/BiJyncjBTdH/?utm_source=ig_share_sheet&igshid=1mbghm6h5zg4v" target="_blank" class="fa fa-instagram icon icon-border dribbble"> </a></li>
					</ul>
					<div class="clearfix"> </div>
				</div> 
				<p>© 2018 Papi's Grill. All rights reserved | Design by <a href="http://greymatteragency.com">Greymatter Agency</a></p>	
			</div>
		</div>
		<!-- //copy-right -->





		<!-- start-smooth-scrolling -->
		<script type="text/javascript" src="js/move-top.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>	
		<script type="text/javascript">
				jQuery(document).ready(function($) {
					$(".scroll").click(function(event){		
						event.preventDefault();
				
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
					});
				});
		</script>
		<script type="text/javascript" src="js/bootstrap.js"></script>
	</body>
</html>