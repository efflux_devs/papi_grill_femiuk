<?PHP
session_start();
include 'admin/library/config.php';
include 'admin/library/functions.php';  

$pageid = MyClass::data_filter($_GET['id']);
$stmt = $DBconnection->prepare("SELECT * FROM pages WHERE id=:id OR pagecode=:id");
$stmt->bindParam(':id', $pageid); 
$stmt->execute();
$pagerow = $stmt->fetch(PDO::FETCH_ASSOC);
$pagetitle = $pagerow['pagename'];
$pagecontent = $pagerow['content'];
?>

<?php echo "$pagecontent"; ?>