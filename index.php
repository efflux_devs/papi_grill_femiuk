<?PHP
session_start();
include 'admin/library/config.php';
include 'admin/library/functions.php';  

$pagename="homepage";
$stmt = $DBconnection->prepare("SELECT * FROM pages WHERE id=:id OR pagecode=:id");
$stmt->bindParam(':id', $pagename); 
$stmt->execute();
$pagerow = $stmt->fetch(PDO::FETCH_ASSOC);
$pagetitle = $pagerow['pagename'];
$pagebanner = $pagerow['pagebanner'];
$pagecontent = $pagerow['content'];

$i= 1;
$statement = $DBconnection->prepare("SELECT * FROM banners ORDER BY id ASC");
$statement->execute();
while($dashboarddatarows = $statement->fetch(PDO::FETCH_ASSOC)) {
$dashboarddata_id = $dashboarddatarows['id'];
$dashboarddata_banner = $dashboarddatarows['image'];
$dashboarddata_link = $dashboarddatarows['link'];

$BannerIMG_[$i] = "$dashboarddata_banner";
$BannerLINK_[$i] = "$dashboarddata_link";
$i++;
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Home - Papi's Grill</title>
		<!-- custom-theme -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="keywords" content="Candid Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
				function hideURLbar(){ window.scrollTo(0,1); } </script>
		<!-- //custom-theme -->
		<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
		<!-- stylesheet -->
		<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
		<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
		<!-- //stylesheet -->
		<!-- online fonts -->
		<link href="//fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/mediaquery.css">
		<link href="//fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
		<!-- //online fonts -->
		<!-- font-awesome-icons -->
		<link href="css/font-awesome.css" type="text/css" rel="stylesheet"> 
		<!-- //font-awesome-icons -->

		<script type="text/javascript" src="js/scroll.js"></script>
		<style type="text/css">
			input {
			  position: fixed;
			  opacity: 0;
			}
			label {
			  position: absolute;
			  margin: 0;
			  padding: 0;
			  border: none;
			  outline: none;
			  background: none;
			  cursor: pointer;
			}
			label::before {
			  position: fixed;
			  z-index: 1;
			  top: 0;
			  left: 0;
			  right: 0;
			  bottom: 0;
			  background-color: rgba(18, 67, 100, .6);
			  content: '';
			  opacity: 0;
			  pointer-events: none;
			  transition: opacity 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger {
			  position: fixed;
			  top: 2em;
			  left: 2em;
			  z-index: 3;
			  width: 4em;
			  height: 3em;
			  margin: 0;
			  padding: 0;
			  transition: opacity 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger::before, label .burger .bar, label .burger::after {
			  position: absolute;
			  left: 0;
			  display: block;
			  width: 100%;
			  height: 12%;
			  background: red;
			  content: '';
			  transition: all 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			label .burger .bar {
			  top: 44%;
			}
			label .burger::before {
			  top: 0;
			  transform-origin: top left;
			}
			label .burger::after {
			  bottom: 0;
			  transform-origin: bottom left;
			}
			input:focus + label .burger, label:hover .burger {
			  opacity: 0.75;
			}
			nav {
			  position: fixed;
			  top: 0;
			  left: 0;
			  bottom: 0;
			  z-index: 2;
			  display: flex;
			  flex-flow: column nowrap;
			  justify-content: center;
			  align-items: flex-start;
			  transform: translate3d(0, 0, 0);
			  /* material acceleration */
			  transform: translateX(-100%);
			  will-change: transform;
			  transition: transform 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			nav::before {
			  position: absolute;
			  top: 0;
			  bottom: 0;
			  left: 0;
			  z-index: -1;
			  width: 25em;
			  background: #d62956;
			  content: '';
			  transform: skewX(15deg) translateX(-100%);
			  transform-origin: bottom left;
			  will-change: transform;
			  transition: transform 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			@media (min-width: 40em) {
			  nav::before {
			    width: 40em;
			  }
			}
			nav a {
			  margin: 0.3em 0;
			  padding: 0em 2em;
			  font-size: 2em;
			  color: white;
			  text-decoration: none;
			  font-weight: 600;
			  transform: translateX(-100%);
			  transition: color 0.15s, transform 0.5s;
			  transition-timing-function: cubic-bezier(0.19, 1, 0.22, 1);
			}
			nav a:nth-child(1) {
			  transition-delay: 0s, 100ms;
			}
			nav a:nth-child(2) {
			  transition-delay: 0s, 150ms;
			}
			nav a:nth-child(3) {
			  transition-delay: 0s, 200ms;
			}
			nav a:nth-child(4) {
			  transition-delay: 0s, 250ms;
			}
			nav a:nth-child(5) {
			  transition-delay: 0s, 300ms;
			}
			nav a:nth-child(6) {
			  transition-delay: 0s, 350ms;
			}
			nav a:hover, nav a:focus {
			  color: black;
			}
			main {
			  overflow: hidden;
			}
			main .content {
			  transform: translate3d(0, 0, 0);
			  /* material acceleration */
			  will-change: transform, filter;
			  transition: all 0.5s cubic-bezier(0.19, 1, 0.22, 1);
			}
			[id="navcheck"]:checked + label::before {
			  opacity: 1;
			  pointer-events: auto;
			}
			[id="navcheck"]:checked + label .burger::before, [id="navcheck"]:checked + label .burger::after {
			  width: 141.42%;
			}
			[id="navcheck"]:checked + label .burger::before {
			  transform: rotate(45deg) translateY(-50%);
			}
			[id="navcheck"]:checked + label .burger::after {
			  transform: rotate(-45deg) translateY(50%);
			}
			[id="navcheck"]:checked + label .burger .bar {
			  transform: scale(0.1);
			}
			[id="navcheck"]:checked ~ nav {
			  transform: translateX(0);
			}
			[id="navcheck"]:checked ~ nav::before {
			  transform: skewX(15deg) translateX(0);
			}
			[id="navcheck"]:checked ~ nav a {
			  transform: translateX(0);
			}
			[id="navcheck"]:checked ~ main .content {
			  transform: translateX(3em);
			  transform-origin: left center;
			  -webkit-filter: blur(2px);
			  filter: blur(2px);
			  /* quite laggy :S */
			}
			/* helper */
			.visuallyhidden {
			  border: 0;
			  clip: rect(0 0 0 0);
			  height: 1px;
			  margin: -1px;
			  overflow: hidden;
			  padding: 0;
			  position: absolute;
			  width: 1px;
			}
			/* misc */
			body {
			  overflow-x: hidden;
			  background: #444;
			  color: white;
			  font: 1em/1.4 "lato";
			}
			main {
			  background: #3498db;
			  padding: 5% 0;
			}
			article {
			  width: 80%;
			  margin: 0 auto;
			  font-size: 1.3em;
			}
			@media (min-width: 60em) {
			  article {
			    width: 50%;
			  }
			}
			h1 {
			  font: 2.5em/1.4 "roboto condensed";
			  font-weight: 700;
			}
		</style>
	</head>
	<body>
		<!-- BEGIN HEADER -->
			<input type="checkbox" id="navcheck" role="button" title="menu">
			<label for="navcheck" aria-hidden="true" title="menu">
				<span class="burger">
					<span class="bar">
						<span class="visuallyhidden">Menu</span>
					</span>
				</span>
			</label>
			<nav id="menu">
				<img src="logo.png" style="object-fit: cover;height: 145px;" class="img-logo">
				<a href="index.php">WELCOME</a>
				<a href="about.php">OUR STORY</a>
				<a href="main_gallery.php">GALLERY</a>
				<a href="contact.php">CONTACT US</a>
			</nav>
		<!-- END HEADER -->

		<!-- BEGIN VIDEO -->
			<div style="height: 630px; width: 100%;" class="media-video">
				<!-- <video width="100%" height="" loop autoplay>
					<source src="video/papi_grill.mp4" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'> 
					<source src="video/papi_grill.ogv" type='video/ogg; codecs="theora, vorbis"'> 
					<object width="640" height="384" type="application/x-shockwave-flash" data="video/papi_grill.mp4"> 
						<param name="movie" value="video/papi_grill.mp4" /> 
					</object> 
				</video> -->

				<video width="100%" height="" loop controls="" autoplay class="video_parent video_parent_for_web video_parent_for_web_height">
					<!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
				    <source type="video/webm" src="video/papi_grill.webm" />

				    <!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
				    <source type="video/mp4" src="video/papi_grill.mp4" />
				    
				    <!-- M4V for Apple -->
				    <source type="video/mp4" src="video/papi_grill.m4v" />

				    <!-- Ogg/Vorbis for older Firefox and Opera versions -->
				    <source type="video/ogg" src="video/papi_grill.ogv" />
				    
				    <!-- Flash fallback for non-HTML5 browsers without JavaScript -->
				    <object width="100%" height="400" type="application/x-shockwave-flash" data="flashmediaelement.swf">
				        <param name="movie" value="flashmediaelement.swf" />
				        <param name="flashvars" value="controls=true&file=video/papi_grill.mp4" />
				    </object>
				</video>

				<video width="100%" height="" preload="none" controls="" loop autoplay class="video_parent video_parent_for_mobile">
				    <!-- MP4 for Safari, IE9, iPhone, iPad, Android, and Windows Phone 7 -->
				    <source type="video/mp4" src="video/papi_grill.mp4" />
				    <!-- WebM/VP8 for Firefox4, Opera, and Chrome -->
				    <source type="video/webm" src="video/papi_grill.webm" />
				    <!-- M4V for Apple -->
				    <source type="video/mp4" src="video/papi_grill.m4v" />
				    <!-- Ogg/Vorbis for older Firefox and Opera versions -->
				    <source type="video/ogg" src="video/papi_grill.ogv" />
				    
				    <!-- Flash fallback for non-HTML5 browsers without JavaScript -->
				    <object width="100%" height="400" type="application/x-shockwave-flash" data="flashmediaelement.swf">
				        <param name="movie" value="flashmediaelement.swf" />
				        <param name="flashvars" value="controls=true&file=video/papi_grill.mp4" />
				    </object>
				</video>
			</div>
		<!-- END VIDEO -->
		
		<!-- home -->
		<div class="home_ w3layouts">
		 	<div class="home_grids_w3">
			    <div class="home_main">
				   <div class="col-md-7 col-sm-7 col-xs-7 img1 img-grid" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[1]"; ?>) center;">
					   <div class="img_text_w3ls text-center">
							<h4></h4>
							<p></p>
						</div>
					</div>
					<div class="col-md-5 img2 col-sm-5 col-xs-5 img-grid" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[2]"; ?>) center;">
					    <div class="img_text_w3ls text-center">
							<h4></h4>
							<p></p>
						  </div>
					</div>
					<div class="clearfix"></div>
				</div>
			    <div class="home_main">
				    <div class="col-md-5 col-sm-5 col-xs-5 img3 img-grid" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[3]"; ?>) center;">
					    <div class="img_text_w3ls text-center">
							
					  	</div>
					</div>
				    <div class="col-md-7 col-sm-7 col-xs-7 img4 img-grid" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[4]"; ?>) center;">
					    <div class="img_text_w3ls text-center">
							
						</div>
				    </div>
				    <div class="clearfix"></div>
			    </div>
			    <div class="home_main">
				   <div class="col-md-7 col-sm-7 col-xs-7 img-grid  img5" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[5]"; ?>) center;">
					  <div class="img_text_w3ls text-center">
							
					  </div>
					 </div>
					<div class="col-md-5 col-sm-5 col-xs-5 img-grid img6" style="background: url(<?php echo "$SiteURL"; ?><?php echo "$BannerIMG_[6]"; ?>) center;">
					  <div class="img_text_w3ls text-center">
							
					  </div>
					 </div>
					<div class="clearfix"></div>
			    </div>
		    </div>
		</div>
		<!-- //home -->

		
		<!-- section -->
		<?php echo "$pagecontent"; ?>
		<!-- //section --> 
		 
		 
		<!-- subscribe -->
		<div class="subscribe w3_agile">
		 	
		</div>
		<!-- //subscribe --> 
		<!-- copy-right -->
		<div class="copy-right agileits-w3layouts">
			<div class="container">
				<div class="social-icons agileits">
		     		<ul>
						<li><a href="https://www.facebook.com/papis.grill.7" target="_blank" class="fa fa-facebook icon icon-border facebook"> </a></li>
						<li><a href="https://twitter.com/PapisGrilLondon?s=08" target="_blank" class="fa fa-twitter icon icon-border twitter"> </a></li>
						<li><a href="https://www.instagram.com/p/BiJyncjBTdH/?utm_source=ig_share_sheet&igshid=1mbghm6h5zg4v" target="_blank" class="fa fa-instagram icon icon-border dribbble"> </a></li>
					</ul>
					<div class="clearfix"> </div>
				</div> 
				<p>© 2018 Papi's Grill. All rights reserved | Design by <a href="http://greymatteragency.com">Greymatter Agency</a></p>	
			</div>
		</div>
		<!-- //copy-right -->
		
		<!-- start-smooth-scrolling -->
		<script type="text/javascript" src="js/move-top.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>	
		<script type="text/javascript" src="js/bootstrap.js"></script>
	</body>
</html>